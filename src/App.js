import './App.css';
import { TaskList } from './components/TaskList/TaskList';
import { Provider } from './context/TasksContext';

function App() {
  return (
    <Provider>
      <div className="tasklist-app">
        <TaskList />
      </div>
    </Provider>
  );
}

export default App;
