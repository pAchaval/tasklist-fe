import React from "react";
import './Button.css';

export const Button = ({ disabled, action, label }) => {
    return <button className="button" disabled={disabled} onClick={action}>{label}</button>
}