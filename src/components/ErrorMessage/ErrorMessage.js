import React from "react";
import './ErrorMessage.css';

export const ErrorMessage = ({ errorMessage, isLoading }) => {
    if (!errorMessage || isLoading) return null;

    return <span className="error-message">{errorMessage}</span>
}
