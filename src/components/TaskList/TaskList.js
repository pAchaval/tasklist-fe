import React, { useContext, useState } from "react";
import { Context as TasksContext } from "../../context/TasksContext";
import { Button } from "../Button/Button";
import { ErrorMessage } from "../ErrorMessage/ErrorMessage";
import { Input } from "../Input/Inputs";
import { Loader } from "../Loader/Loader";
import { TaskDetail } from "../TaskDetail/TaskDetail";
import { TaskItem } from "../TaskItem/TaskItem";
import './TaskList.css';

export const TaskList = () => {
    const { state, getTasks, addTitles, removeTasks, openDetail, completeTask } = useContext(TasksContext);
    const [qty, setQty] = useState('');
    const [limit, setLimit] = useState('');

    return (
        <>
            <div className="controls">

                <Input placeholder='Enter number of titles' setValue={setQty} />
                <Button label='Add Titles' disabled={qty === ''} action={() => addTitles(qty)} />

                <Input placeholder='Enter number of tasks' setValue={setLimit} />
                <Button label='Get Tasks' disabled={limit === ''} action={() => getTasks(limit)} />

                <Button label='Remove Tasks' disabled={state.tasks.length === 0} action={() => removeTasks()}/>

            </div>

            <TaskDetail
                openDetail={openDetail}
                completeTask={completeTask}
                detailOpen={state.detailOpen}
            />

            <Loader isLoading={state.isLoading} />

            <ErrorMessage isLoading={state.isLoading} errorMessage={state.errorMessage} />

            {!state.isLoading && (
                <div className="tasks-container">

                    {state.tasks.map((task, index) => (
                        <TaskItem task={task} index={index} action={openDetail}/>
                    ))}

                </div>
            )}

        </>
    )
};

