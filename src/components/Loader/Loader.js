import React from "react";
import './Loader.css';

export const Loader = ({ isLoading }) => {

    if (!isLoading) return null;

    return <span className="loader">Loading...</span>
}
