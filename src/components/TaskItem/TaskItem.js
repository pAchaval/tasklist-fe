import React from "react";

export const TaskItem = ({task, index, action}) => {
    return (
        <div onClick={() => action({ open: true, data: { task, index } })}>
            <h5 className="text">Task #{index + 1}</h5>
            <h5 className="text">{task.title}</h5>
        </div>
    )
}
