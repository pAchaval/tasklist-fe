import React from "react";
import './Input.css';

export const Input = ({ placeholder, setValue }) => {
    return (
        <input className='input' placeholder={placeholder} onChange={(e) => setValue(e.target.value)} />
    )

}
