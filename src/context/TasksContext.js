import tasksApi from "../api/tasksApi";
import createDataContext from "./createDataContext";


const taskReducer = (state, action) => {
    switch (action.type) {
        case "SET_LOADING":
            return {
                ...state,
                isLoading: action.payload,
            }
        case "SET_RESULTS":
            return {
                ...state,
                tasks: action.payload,
            }
        case "SET_ERROR":
            return {
                ...state,
                errorMessage: action.payload,
            }
        case "OPEN_DETAIL":
            return {
                ...state,
                detailOpen: action.payload,
            }
        default:
            break;
    }
}

const addTitles = dispatch => async (qty) => {
    dispatch({ type: 'SET_LOADING', payload: true });
    const queryParam = qty === '' ? '3' : qty;
    try {
        const response = await tasksApi.get(`/tasks?numberOfTasks=${queryParam}`);
        dispatch({ type: 'SET_RESULTS', payload: response.data });
        dispatch({ type: 'SET_ERROR', payload: undefined })
        dispatch({ type: 'SET_LOADING', payload: false });
    } catch (err) {
        dispatch({ type: 'SET_RESULTS', payload: [] });
        dispatch({ type: 'SET_ERROR', payload: err.message })
        dispatch({ type: 'SET_LOADING', payload: false });
    }
}

const getTasks = dispatch => async (limit) => {
    dispatch({ type: 'SET_LOADING', payload: true });
    try {
        const response = await tasksApi.get(`/tasks?savedTasks=${limit}`);
        dispatch({ type: 'SET_RESULTS', payload: response.data });
        dispatch({ type: 'SET_ERROR', payload: undefined })
        dispatch({ type: 'SET_LOADING', payload: false });
    } catch (err) {
        dispatch({ type: 'SET_RESULTS', payload: [] });
        dispatch({ type: 'SET_ERROR', payload: err.message })
        dispatch({ type: 'SET_LOADING', payload: false });
    }
}

const removeTasks = dispatch => async () => {
    dispatch({ type: 'SET_LOADING', payload: true });
    try {
        await tasksApi.get('/tasks/remove');
        dispatch({ type: 'SET_LOADING', payload: false });
        dispatch({ type: 'SET_RESULTS', payload: [] });
        dispatch({ type: 'SET_ERROR', payload: undefined })
    } catch (err) {
        dispatch({ type: 'SET_LOADING', payload: false });
        dispatch({ type: 'SET_ERROR', payload: err.message })
    }
};

const completeTask = dispatch => async (task) => {
    dispatch({ type: 'SET_LOADING', payload: true });
    try {
        await tasksApi.put(`/tasks?id=${task._id}`);
        dispatch({ type: 'SET_LOADING', payload: false });
        dispatch({ type: 'SET_RESULTS', payload: [] });
        dispatch({ type: 'SET_ERROR', payload: undefined })
    } catch (err) {
        dispatch({ type: 'SET_LOADING', payload: false });
        dispatch({ type: 'SET_ERROR', payload: err.message })
    } finally {
        dispatch({ type: 'OPEN_DETAIL', payload: { open: false, data: {} } })
    }
};

const openDetail = dispatch => ({ open, data }) => {
    dispatch({ type: 'OPEN_DETAIL', payload: { open, data } })
};

export const { Provider, Context } = createDataContext(
    taskReducer,
    { addTitles, getTasks, removeTasks, openDetail, completeTask },
    {
        isLoading: false,
        detailOpen: { open: false, data: {} },
        errorMessage: undefined,
        tasks: [],
    }
)