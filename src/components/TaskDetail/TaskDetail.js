import React from "react";
import './TaskDetail.css';

export const TaskDetail = ({ openDetail, completeTask, detailOpen }) => {
    return (
        <div className="centerpoint">

            <dialog className="dialog" open={detailOpen.open}>

                <h5 className="text">Task #{detailOpen.data.index + 1} - {detailOpen.data.task?.title}</h5>
                <button onClick={() => openDetail({ open: false, data: {} })}>Close</button>
                <button onClick={() => completeTask(detailOpen.data.task)}>Complete</button>

            </dialog>

        </div>
    )
};
